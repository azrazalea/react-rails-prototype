class SpellUse < ApplicationRecord
  belongs_to :spell
  has_many :spell_results
end
