import * as React from "react";

interface P {
  title: string;
  body: string;
  uses: Array<{ purpose: string; created_at: string; }>;
}
interface S {}

class Spell extends React.Component<P, S> {
  constructor(props: P) {
    super(props);
  }

  render () {
    return (
      <div>
        <h3>{this.props.title}</h3>
        <div>{this.props.body}</div>
        {this.props.uses.length > 0 && <h4>Uses</h4>}
        {<ul>
             {this.props.uses.map((use) => {
                return <li>{use.purpose} {use.created_at}</li>;
             })}
           </ul>}
      </div>
    );
  }
}

export default Spell;
