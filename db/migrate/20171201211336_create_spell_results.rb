class CreateSpellResults < ActiveRecord::Migration[5.1]
  def change
    create_table :spell_results do |t|
      t.text :description
      t.timestamps
    end
    add_reference :spell_results, :spell_use, foreign_key: true
  end
end
