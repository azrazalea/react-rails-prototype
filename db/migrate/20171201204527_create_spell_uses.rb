class CreateSpellUses < ActiveRecord::Migration[5.1]
  def change
    create_table :spell_uses do |t|
      t.text :purpose
      t.timestamps
    end
    add_reference :spell_uses, :spell, foreign_key: true
  end
end
